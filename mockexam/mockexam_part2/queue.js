let collection = [];

// Write the queue functions below.


// output all the elements of the queue
function print() {
    return collection;
}

// add element to rear of queue
function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}

// remove element at front of queue
function dequeue() {
    let newCollection = [];

    for (i = 0; i < collection.length-1; i++){
        newCollection[i] = collection[i+1];
    }
    return collection = newCollection
}

// show element at the front
function front() {
    return collection[0];
}

// show total number of elements
function size() {
    return collection.length;
}

// outputs Boolean value describing whether queue is empty or not
function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};